<?php

namespace App\Services;

use App\Entity\Notifications;
use Symfony\Bridge\Doctrine\RegistryInterface;

class NotificationService {

    public $manager;

    public function __construct(RegistryInterface $manager) {
        $this->manager = $manager;
    }

    public function set($user, $type, $typeId, $extra=null) {

        $em = $this->manager->getManager();

        $notification = new Notifications();

        $notification->setUser($user);
        $notification->setType($type);
        $notification->setTypeId($typeId);
        $notification->setReaded(0);
        $notification->setCreatedAt(new \DateTime('now'));
        $notification->setExtra($extra);
        
        $em->persist($notification);
        $flush=$em->flush();
        
        if($flush==null){
            $status=true;
        }else{
            $status=false;
        }
        
        return $status;
    }
    
    
    public function read($user){
        
        $em= $this->manager->getManager();
        
        $notificcation_repo=$em->getRepository(Notifications::class);
        $notifications=$notificcation_repo->findBy(['user'=>$user]);
        
        foreach($notifications as $notification){
            
            $notification->setReaded(1);
            $em->persist($notification);
        }
        $flush=$em->flush();
        
        if($flush==null){
            $status='marcadas como leidas' ;
        }else{
            $status='no se han marcado como leidas';
        }
        
        return $status;
    }

}
