<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
//Para codificar el password:
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use Symfony\Component\HttpFoundation\Response;
//Para las sesiones de usuario:
use Symfony\Component\HttpFoundation\Session\Session;
//para manejar los datos enviados por formulario:
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
//entidad de users:
use App\Entity\Users;
//clase del formulario de registro usuarios:
use App\Form\RegisterType;

use App\Form\UserType;

class UserController extends Controller {

    private $session;

    public function __construct() {
        $this->session = new Session();
    }

    /**
     * @Route("/user", name="user")
     */
    public function index() {
        return $this->render('user/index.html.twig', [
                    'controller_name' => 'UserController',
        ]);
    }

    public function login(AuthenticationUtils $auth) {
        //redirigir si estás logueado y se intenta entrar por la ruta
        if(is_object($this->getUser())){
            return $this->redirectToRoute('publication_index');
        }
        
        //variable en la que guardamos el error en caso de que lo haya
        $error=$auth->getLastAuthenticationError();
        
        //variable para guardar el nombre del usuario que intentó autenticarse y ha fallado
        $lastUsername=$auth->getLastUsername();
        
        return $this->render('user/login.html.twig',[
            'error'=>$error,
            'lastUsername'=>$lastUsername,
        ]);
    }

    public function register(Request $request, UserPasswordEncoderInterface $encoder) {
        
        //redirigir si estás logueado y se intenta entrar por la ruta
        if(is_object($this->getUser())){
            return $this->redirectToRoute('publication_index');
        }

        $user = new Users();
        $form = $this->createForm(RegisterType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if($form->isValid()) {
                $user_repo = $this->getDoctrine()->getRepository(Users::class);
                $user_isset = $user_repo->comprobarEmailNick($form->get('email')->getData(), $form->get('nick')->getData());

                if (count($user_isset) == 0) {

                    $encoded = $encoder->encodePassword($user, $user->getPassword());
                    $user->setPassword($encoded);
                    $user->setRole('ROLE_USER');

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($user);
                    $flush = $em->flush();

                    if ($flush == null) {
                        $status = "Te has registrado correctamente";
                        $this->session->getFlashBag()->add('status', $status);
                        return $this->redirect('login');
                    } else {
                        $status = "NO te has registrado";
                    }
                } else {
                    $status = "El usuario ya existe";
                }
            } else {
                $status = "No te has registrado correctamente";
            }
            $this->session->getFlashBag()->add('status', $status);
        }
        return $this->render('user/register.html.twig', [
                    'form' => $form->createView(),
        ]);
    }
    
    //función para comprobar si el nick ya existe con una petición Ajax 
    public function nickTest(Request $request){
        
        $nick=$request->get('nick');
        $user_repo= $this->getDoctrine()->getRepository(Users::class);
        $user_isset=$user_repo->findOneBy(['nick'=>$nick]);
        if($user_isset!=null){       
            $usado="used";
            
        }else{
            $usado="unused";
        
        }
        
        return new Response($usado);
    }
    
    public function emailTest(Request $request){
        
        $email=$request->get('email');
        $user_repo= $this->getDoctrine()->getRepository(Users::class);
        $email_isset=$user_repo->findOneBy(['email'=>$email]);
        if($email_isset!=null){
            $usado="used";
        }else{
            $usado="unused";
        }
        
        return new Response($usado);
    }
    
    public function editUser(Request $request){
        
        $user=$this->getUser();
        $user_image=$user->getImage();
        $form= $this->createForm(UserType::class,$user);
        
        
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if($form->isValid()) {
                $user_repo = $this->getDoctrine()->getRepository(Users::class);
                $user_isset = $user_repo->comprobarEmailNick($form->get('email')->getData(), $form->get('nick')->getData());
                

                if (count($user_isset) == 0 || $user->getEmail()==$user_isset[0]->getEmail() && $user->getNick()==$user_isset[0]->getNick()) {

                    //subir imagen:
                    $file=$form["image"]->getData();
                    
                    if(!empty($file) && $file !=null){
                        $ext=$file->guessExtension();
                        if($ext=="jpg" || $ext=="jpeg" || $ext=="gif" || $ext=="png"){
                            $file_name=$user->getId().time().'.'.$ext;
                            $file->move("uploads/users",$file_name);
                            $user->setImage($file_name);
                        }
                        
                    }else{
                        $user->setImage($user_image);
                    }
                    
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($user);
                    $flush = $em->flush();

                    if ($flush == null) {
                        $status = "Has modificado tus datos correctamente";
                        $this->session->getFlashBag()->add('status', $status);
                        return $this->redirect('login');
                    } else {
                        $status = "NO se han modificado tus datos";
                    }
                } else {
                    $status = "El usuario ya existe";
                }
            } else {
                $status = "Error: No se han modificado tus datos";
            }
            $this->session->getFlashBag()->add('status', $status);
            return $this->redirectToRoute('user-edit');
        } 
        
        return $this->render('user/edit_user.html.twig',[
            'form'=>$form->createView(),
        ]);
    }
    
    public function listUsers(Request $request){
        
        $em=$this->getDoctrine()->getManager();
        $dql="SELECT u FROM App\Entity\Users u ORDER BY u.id";
        $query=$em->createQuery($dql);
        
        
        $paginator= $this->get('knp_paginator');
        $pagination=$paginator->paginate($query,$request->query->getInt('page',1),5);
        
        return $this->render('/user/list_users.html.twig',[
            'pagination'=>$pagination,
        ]);
    }
    
    public function searchUsers(Request $request){
        
        $search= trim($request->get('busqueda'));
        
        if($search==''){
            return $this->redirectToRoute('publication_index');
        }
        
        $em=$this->getDoctrine()->getManager();
        $dql="SELECT u FROM App\Entity\Users u WHERE u.name LIKE :search OR u.surname LIKE :search OR u.nick LIKE :search ORDER BY u.id";
        $query=$em->createQuery($dql);
        $query->setParameter('search','%'.$search.'%');
        
        
        $paginator= $this->get('knp_paginator');
        $pagination=$paginator->paginate($query,$request->query->getInt('page',1),5);
        
        return $this->render('/user/list_users.html.twig',[
            'pagination'=>$pagination,
        ]);     
    }
    
    public function profile(Request $request,$nickname=null){
        
        $em= $this->getDoctrine()->getManager();
        
        if($nickname!=null){
            $user_repo=$em->getRepository(Users::class);
            $user=$user_repo->findOneBy(['nick'=>$nickname]);
        }else{
            $user= $this->getUser();     
        }
        
        if(empty($user) || !is_object($user)){
            
            return $this->redirectToRoute('publication_index');
        }
     
        $user_id=$user->getId();
        
        $dql="SELECT p FROM App\Entity\Publications p WHERE p.user=$user_id ORDER BY p.id DESC";
        
        $query=$em->createQuery($dql);
        
        $paginator= $this->get('knp_paginator');
        $publications=$paginator->paginate(
                $query,
                $request->query->getInt('page',1),
                5);
                
        return $this->render('user/profile.html.twig',[
            'user'=>$user,
            'publications'=>$publications,
        ]);
        
        
    }

}
