<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
//Entidades:
use App\Entity\Users;
use App\Entity\PrivateMessages;
//formulario:
use App\Form\PrivateMessageType;

class PrivateMessageController extends Controller {

    private $session;

    public function __construct() {
        $this->session = new Session();
    }

    public function index(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $private_message = new PrivateMessages();
        $form = $this->createForm(PrivateMessageType::class, $private_message, ['empty_data' => $user]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                //subir imagen
                $file = $form['image']->getData();
                if (!empty($file) && $file != null) {
                    $ext = $file->guessExtension();
                    if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif') {
                        $file_name = $user->getId() . time() . "." . $ext;
                        $file->move('uploads/messages/images', $file_name);
                        $private_message->setImage($file_name);
                    }
                } else {
                    $private_message->setImage(null);
                }

                //subir documento
                $doc = $form['file']->getData();
                if (!empty($doc) && $doc != null) {
                    $ext = $doc->guessExtension();
                    if ($ext == 'pdf' || $ext == 'doc') {
                        $file_name = $user->getId() . time() . "." . $ext;
                        $doc->move('uploads/messages/documents', $file_name);
                        $private_message->setFile($file_name);
                    }
                } else {
                    $private_message->setFile(null);
                }

                $private_message->setEmitter($user);
                $private_message->setCreatedAt(new \DateTime("now"));
                $private_message->setReaded(0);
                $em->persist($private_message);
                $flush = $em->flush();
                if ($flush == null) {
                    $status = "Mensaje enviado correctamente";
                } else {
                    $status = "Error al enviar el mensaje";
                }
            } else {
                $status = "No se ha podido enviar el mensaje, el formulario no es válido";
            }
            $this->session->getFlashBag()->add('status', $status);
            return $this->redirectToRoute('private_messages');
        }

        $private_messages = $this->getPrivateMessages($request);
        $this->setReaded($em, $user);
        
        return $this->render('privateMessage/index.html.twig', [
                    'form' => $form->createView(),
                    'pagination'=>$private_messages,
        ]);
    }

    public function sended(Request $request) {
        $private_messages = $this->getPrivateMessages($request, 'sended');
        
        return $this->render('privateMessage/sended.html.twig',[
            'pagination'=>$private_messages,
        ]);
    }

    public function getPrivateMessages($request, $type = null) {

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $user_id = $user->getId();

        if ($type == 'sended') {
            $dql = "SELECT p FROM App\Entity\PrivateMessages p WHERE p.emitter=$user_id ORDER BY p.id DESC";
        } else {
            $dql = "SELECT p FROM App\Entity\PrivateMessages p WHERE p.receiver=$user_id ORDER BY p.id DESC";
        }

        $query = $em->createQuery($dql);
        
        $paginator = $this->get('knp_paginator');
        // Paginate the results of the query
        $pagination = $paginator->paginate(
                // Doctrine Query, not results
                $query,
                // Define the page parameter
                $request->query->getInt('page', 1),
                // Items per page
                5
        );
        
        return $pagination;
    }
    
    
    public function NotReaded(){      
        $em= $this->getDoctrine()->getManager();
        $messages_repo=$em->getRepository(PrivateMessages::class);
        $user= $this->getUser();
        
        $messages=$messages_repo->findBy(['receiver'=>$user,'readed'=>0]);
        
        $num_messages= count($messages);
        
        return new Response($num_messages);
        
    }
    
    public function setReaded($em,$user){
        
        $messages_repo=$em->getRepository(PrivateMessages::class);
        $messages=$messages_repo->findBy(['receiver'=>$user,'readed'=>0]);
        
        foreach($messages as $msg){
            
            $msg->setReaded(1);
            $em->persist($msg);
        }
        $flush=$em->flush();
        
        if($flush==null){
            $result=true;
        }else{
            $result=false;
        }
       
        return $result;
    }

}
