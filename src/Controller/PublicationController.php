<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Publications;
use App\Form\PublicationType;

class PublicationController extends Controller {

    public function index(Request $request, Session $session) {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $publication = new Publications();
        $form = $this->createForm(PublicationType::class, $publication);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                //subir imagen
                $file = $form['image']->getData();
                if (!empty($file) && $file != null) {
                    $ext = $file->guessExtension();
                    if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif') {
                        $file_name = $user->getId() . time() . "." . $ext;
                        $file->move('uploads/publications/images', $file_name);
                        $publication->setImage($file_name);
                    }
                } else {
                    $publication->setImage(null);
                }

                //subir documento
                $doc = $form['document']->getData();
                if (!empty($doc) && $doc != null) {
                    $ext = $doc->guessExtension();
                    if ($ext == 'pdf' || $ext == 'doc') {
                        $file_name = $user->getId() . time() . "." . $ext;
                        $doc->move('uploads/publications/documents', $file_name);
                        $publication->setDocument($file_name);
                    }
                } else {
                    $publication->setDocument(null);
                }

                $publication->setUser($user);
                $publication->setCreatedAt(new \DateTime("now"));
                $em->persist($publication);
                $flush = $em->flush();
                if ($flush == null) {
                    $status = "Publicación creada correctamente";
                } else {
                    $status = "Error al añadir la publicación";
                }
            } else {
                $status = "No se ha podido crear la publicación, el formulario no es válido";
            }
            $session->getFlashBag()->add('status', $status);
            return $this->redirectToRoute('publication_index');
        }
        $publications = null;
        if ($user != null) {
            $publications = $this->getPublications($request);
        }
        return $this->render('publication/index.html.twig', [
                    'form' => $form->createView(),
                    'publications' => $publications,
        ]);
    }

    public function getPublications(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();
        $publications_repo = $this->getDoctrine()->getRepository(Publications::class);
        $following_repo = $this->getDoctrine()->getRepository(\App\Entity\Following::class);



        $following = $following_repo->findBy(['user' => $user]);

        //Compruebo que sigue a alguien y si no fuera así que no devuelva nada la función
        if (!empty($following)) {
            foreach ($following as $follow) {
                $following_array[] = $follow->getFollowed();
            }

            /* SELECT p.text FROM publications p WHERE p.user_id=1 OR
              p.user_id IN (SELECT followed  FROM following f WHERE f.user_id=1); */

            $query = $publications_repo->createQueryBuilder('p')
                    ->where('p.user=:user_id OR p.user IN (:following) ')
                    ->setParameter('user_id', $user->getId())
                    ->setParameter('following', $following_array)
                    ->orderBy('p.id', 'DESC')
                    ->getQuery();

            $paginator = $this->get('knp_paginator');

            // Paginate the results of the query
            $pagination = $paginator->paginate(
                    // Doctrine Query, not results
                    $query,
                    // Define the page parameter
                    $request->query->getInt('page', 1),
                    // Items per page
                    5
            );

            return $pagination;
        }
    }

    public function removePublication($id = null, Request $request) {

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $publications_repo = $em->getRepository(Publications::class);
        $publication = $publications_repo->find($id);
        if ($publication != null) {
            if ($user->getId() == $publication->getUser()->getId()) {

                $em->remove($publication);
                $flush = $em->flush();
                if ($flush == null) {
                    $status = "Publicación eliminada correctamente";
                } else {
                    $status = "La publicacion no se ha podido borrar";
                }
            } else {
                $status = "La publicacion no se ha podido borrar,usuario denegado";
            }
        } else {
            $status = "La publicacion no existe";
        }

        return new Response($status);
    }

}
