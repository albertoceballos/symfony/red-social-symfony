<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;

use App\Services\NotificationService;

use App\Entity\Users;
use App\Entity\Following;

class FollowingController extends Controller
{
    private $session;
    
    public function __construct() {
        $this->session=new Session;
    }


    public function follow(Request $request, NotificationService $notificationService)
    {
        //consigo el usuario loguedo
        $user= $this->getUser();
        
        //consigo el Id del usuario al que voy a seguir
        $followed_id= $request->get('followed');
        
        //consigo el objeto del usuario que se va a seguir 
        $user_repo= $this->getDoctrine()->getRepository(Users::class);
        $followed=$user_repo->find($followed_id);
        
         //Hago yn follow nuevo y seteo el usuario que sigue y al que se va a seguir
        $following=new Following();    
        $following->setUser($user);
        $following->setFollowed($followed);
        
        //persisto y guardo en la BBDD
        $em= $this->getDoctrine()->getManager();
        $em->persist($following);
        $flush=$em->flush();
        
        if($flush==null){
            $notificationService->set($followed, 'follow', $user->getId());
            $status="Ahora estás siguiendo a este usuario";
        }else{
            $status="No se ha podido seguir al usuario";
        }
        
        
       return new Response($status);
    }
    
    
     public function unfollow(Request $request)
    {
        //consigo el usuario loguedo
        $user= $this->getUser();
        
        //consigo el Id del usuario al que voy a dejar de seguir
        $followed_id= $request->get('followed');
        
        //consigo el objeto del follow guardado que quiero borrar 
        $following_repo= $this->getDoctrine()->getRepository(Following::class);
        $followed=$following_repo->findOneBy(['user'=>$user,'followed'=>$followed_id]);
        
        
        //persisto y guardo en la BBDD
        $em= $this->getDoctrine()->getManager();
        $em->remove($followed);
        $flush=$em->flush();
        
        if($flush==null){
            $status="Has dejado de seguir a este usuario";
        }else{
            $status="No se ha podido dejar de seguir al usuario";
        }
            
       return new Response($status);
    }
    
    /* Acción par mostrar a todos los usuarios que sigue un usuario*/
    
    public function following(Request $request, $nickname=null){
        
         $em= $this->getDoctrine()->getManager();
        
        if($nickname!=null){
            $user_repo=$em->getRepository(Users::class);
            $user=$user_repo->findOneBy(['nick'=>$nickname]);
        }else{
            $user= $this->getUser();     
        }
        
        if(empty($user) || !is_object($user)){
            
            return $this->redirectToRoute('publication_index');
        }
     
        $user_id=$user->getId();
        
        $dql="SELECT f FROM App\Entity\Following f WHERE f.user=$user_id ORDER BY f.id DESC";
        
        $query=$em->createQuery($dql);
        
        $paginator= $this->get('knp_paginator');
        $following=$paginator->paginate(
                $query,
                $request->query->getInt('page',1),
                5);
                
        return $this->render('following/following.html.twig',[
            'profile_user'=>$user,
            'pagination'=>$following,
            'type'=>'following',
        ]);
       
    }
    
    public function followed(Request $request,$nickname=null){
        
        $em= $this->getDoctrine()->getManager();
        
        if($nickname!=null){
            $user_repo=$em->getRepository(Users::class);
            $user=$user_repo->findOneBy(['nick'=>$nickname]);
        }else{
            $user= $this->getUser();     
        }
        
        if(empty($user) || !is_object($user)){
            
            return $this->redirectToRoute('publication_index');
        }
     
        $user_id=$user->getId();
        
        $dql="SELECT f FROM App\Entity\Following f WHERE f.followed=$user_id ORDER BY f.id DESC";
        
        $query=$em->createQuery($dql);
        
        $paginator= $this->get('knp_paginator');
        $followed=$paginator->paginate(
                $query,
                $request->query->getInt('page',1),
                5);
                
        return $this->render('following/following.html.twig',[
            'profile_user'=>$user,
            'pagination'=>$followed,
            'type'=>'followed',
        ]);
            
    }
    
}
