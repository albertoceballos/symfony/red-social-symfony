<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Services\NotificationService;
use App\Entity\Notifications;

class NotificationController extends Controller {

    public function index(Request $request, NotificationService $notificationService) {

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $user_id = $user->getId();

        $dql = "SELECT n FROM App\Entity\Notifications n WHERE n.user=$user_id ORDER BY n.id DESC";

        $query = $em->createQuery($dql);

        $paginator = $this->get('knp_paginator');
        $notification = $paginator->paginate(
                $query, $request->query->getInt('page', 1), 5);

        $notificationService->read($user);
        return $this->render('notification/notification_page.html.twig', [
                    'user' => $user,
                    'pagination' => $notification,
        ]);
    }

    public function countNotifications() {

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $not_repo = $em->getRepository(Notifications::class);
        $notifications = $not_repo->findBy(['user' => $user, 'readed' => 0]);

        return New Response(count($notifications));
    }

    public function deleteNotification($id) {

        $em = $this->getDoctrine()->getManager();

        $notification_repo = $em->getRepository(Notifications::class);
        $notification = $notification_repo->find($id);
        
        $em->remove($notification);
        
        $flush=$em->flush();
        
        if($flush==null){
            $status="notificación borrada";
        }else{
            $status="No se ha podido borrar la notificación";
        }
        
        return new Response($status);
    }

}
