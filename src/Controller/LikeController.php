<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;

//servicio para guardar las notificaciones:
use App\Services\NotificationService;

//Entidades:
use App\Entity\Likes;
use App\Entity\Users;
use App\Entity\Publications;

class LikeController extends Controller {

    public function like(NotificationService $notificationService, $id=null){
        $em= $this->getDoctrine()->getManager();
        $publication_repo=$em->getRepository(Publications::class);
        
        $user= $this->getUser();
        $publication=$publication_repo->find($id);
       
        $like=new Likes();
        $like->setUser($user);
        $like->setPublication($publication);
    
        $em->persist($like);
        $flush=$em->flush();
        
        if($flush==null){
            $notificationService->set($publication->getUser(), 'like', $user->getId(), $publication->getId());
            $status="Te gusta esta publicación";
        }else{
            $status="No se ha podido guardar el me gusta";
        }
        
        return new Response($status);
    }
    
    public function dislike($id=null){
        $user= $this->getUser();
        $em= $this->getDoctrine()->getManager();
        $like_repo=$em->getRepository(Likes::class);
        
        $like=$like_repo->findOneBy(['user'=>$user,'publication'=>$id]);
        
        $em->remove($like);
        $flush=$em->flush();
        
        if($flush==null){
            $status="Ya no te gusta la publicación";
        }else{
            $status="Error al desmarcar el me gusta";
            
        }
        return new Response($status);
    }
    
     public function showLikes(Request $request, $nickname=null){
        
         $em= $this->getDoctrine()->getManager();
        
        if($nickname!=null){
            $user_repo=$em->getRepository(Users::class);
            $user=$user_repo->findOneBy(['nick'=>$nickname]);
        }else{
            $user= $this->getUser();     
        }
        
        if(empty($user) || !is_object($user)){
            
            return $this->redirectToRoute('publication_index');
        }
     
        $user_id=$user->getId();
        
        $dql="SELECT l FROM App\Entity\Likes l WHERE l.user=$user_id ORDER BY l.id DESC";
        
        $query=$em->createQuery($dql);
        
        $paginator= $this->get('knp_paginator');
        $likes=$paginator->paginate(
                $query,
                $request->query->getInt('page',1),
                5);
                
        return $this->render('like/likes.html.twig',[
            'user'=>$user,
            'pagination'=>$likes,
        ]);
       
    }

}
