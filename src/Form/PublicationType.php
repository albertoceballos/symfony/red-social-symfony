<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class PublicationType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->
                 add("text", TextareaType::class, [
                    'label' => 'Mensaje',
                    'required' => false,
                    'attr' => [
                        'class' => 'form-control textarea-form',
                    ]
                ])
                 ->add("image",FileType::class, [
                    'label' => 'Foto',
                    'required' => false,
                    'data_class'=>null,
                    'attr' => [
                        'class' => 'form-control',
                    ]
                ])
                 ->add("document",FileType::class, [
                    'label' => 'Documento',
                    'required' => false,
                    'data_class'=>null,
                    'attr' => [
                        'class' => 'form-control',
                    ]
                ])
                
                
                 ->add("Enviar", SubmitType::class, [
                    'label'=>'Guardar',
                    'attr' => [
                        'class' => 'form-submit btn btn-success',
                        'style'=>'margin-top:20px;'
                    ]
                ]);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => "App\Entity\Publications",
            'method'=>'POST',
        ]);
    }

}
