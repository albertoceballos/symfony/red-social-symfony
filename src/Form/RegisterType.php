<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class RegisterType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add("name", TextType::class, [
                    'label' => 'Nombre',
                    'required' => 'required',
                    'attr' => [
                        'class' => 'form-control',
                    ]
                ])
                ->add("surname", TextType::class, [
                    'label' => 'Apellidos',
                    'required' => 'required',
                    'attr' => [
                        'class' => 'form-control',
                    ]
                ])
                ->add("nick", TextType::class, [
                    'label' => 'Nick',
                    'required' => 'required',
                    'attr' => [
                        'class' => 'form-control',
                    ]
                ])
                ->add("email", EmailType::class, [
                    'label' => 'E-Mail',
                    'required' => 'required',
                    'attr' => [
                        'class' => 'form-control',
                    ]
                ])
                ->add("password", PasswordType::class, [
                    'label' => 'Contraseña',
                    'required' => 'required',
                    'attr' => [
                        'class' => 'form-control',
                    ]
                ])
                ->add("submit", SubmitType::class, [
                    'label'=>'Registrar',
                    'attr' => [
                        'class' => 'form-submit btn btn-success',
                    ]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => "App\Entity\Users",
            'method'=>'POST',
        ]);
    }

}
