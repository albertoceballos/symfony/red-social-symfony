<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Doctrine\ORM\EntityRepository;

class PrivateMessageType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $user=$options['empty_data'];
        $builder->
                add("receiver", EntityType::class,[
                    'label'=>'Destinatario',
                    'class'=> \App\Entity\Users::class,
                    'query_builder'=>function(EntityRepository $er) use($user){
                        return $er->getFollowingUser($user);
                    },
                    'choice_label'=>function($user){
                        return $user->getName()." ".$user->getSurname()." ".$user->getNick();
                    },
                ])
                -> add("message", TextareaType::class, [
                    'label' => 'Mensaje',
                    'required' => false,
                    'attr' => [
                        'class' => 'form-control textarea-form',
                    ]
                ])
                 ->add("image",FileType::class, [
                    'label' => 'Imagen',
                    'required' => false,
                    'data_class'=>null,
                    'attr' => [
                        'class' => 'form-control',
                        'style'=>'padding:0px',
                    ]
                ])
                 ->add("file",FileType::class, [
                    'label' => 'Archivo',
                    'required' => false,
                    'data_class'=>null,
                    'attr' => [
                        'class' => 'form-control',
                        'style'=>'padding:0px',
                    ]
                ])
                
                
                 ->add("Enviar", SubmitType::class, [
                    'label'=>'Enviar',
                    'attr' => [
                        'class' => 'form-submit btn btn-success',
                        'style'=>'margin-top:20px;'
                    ]
                ]);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => "App\Entity\PrivateMessages",
            'method'=>'POST',
        ]);
    }

}
