<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Symfony\Bridge\Doctrine\RegistryInterface;

class FollowingExtension extends AbstractExtension
{
    protected $doctrine;

    public function __construct(RegistryInterface $doctrine) {
        $this->doctrine=$doctrine;
    }

    public function getFilters()
    {
        return [
            new TwigFilter('following', [$this, 'followingFilter']),
        ];
    }

    public function followingFilter($user, $followed)
    {
        $following_repo= $this->doctrine->getRepository(\App\Entity\Following::class);
        $user_following=$following_repo->findOneBy([
           "user"=>$user,
           "followed"=>$followed,
        ]);
        if(!empty($user_following) && is_object($user_following)){
            $result=true;   
        }else {
            $result=false;
        }
        return $result;
    }
}
