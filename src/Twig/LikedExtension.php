<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Symfony\Bridge\Doctrine\RegistryInterface;

use App\Entity\Likes;

class LikedExtension extends AbstractExtension
{
    protected $doctrine;

    public function __construct(RegistryInterface $doctrine) {
        $this->doctrine=$doctrine;
    }

    public function getFilters()
    {
        return [
            new TwigFilter('liked', [$this, 'likedFilter']),
        ];
    }

    public function likedFilter($user, $publication)
    {
        $like_repo=$this->doctrine->getRepository(Likes::class);
        
        $publication_liked=$like_repo->findOneBy(['user'=>$user,'publication'=>$publication]);
        
        if(!empty($publication_liked) && is_object($publication_liked)){
            
            $result=true;
        }else{
            $result=false;
        }
        
        return $result;
    }
}
