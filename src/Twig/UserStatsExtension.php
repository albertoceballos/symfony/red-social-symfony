<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Symfony\Bridge\Doctrine\RegistryInterface;

use App\Entity\Following;
use App\Entity\Likes;
use App\Entity\Publications;

class UserStatsExtension extends AbstractExtension
{
    protected $doctrine;

    public function __construct(RegistryInterface $doctrine) {
        $this->doctrine=$doctrine;
    }

    public function getFilters()
    {
        return [
            new TwigFilter('stats', [$this, 'statsFilter']),
        ];
    }

    public function statsFilter($user)
    {
        $following_repo= $this->doctrine->getRepository(Following::class);
        $publications_repo= $this->doctrine->getRepository(Publications::class);
        $likes_repo= $this->doctrine->getRepository(Likes::class);
        
        $siguiendo=$following_repo->findBy(['user'=>$user]);
        $seguidores=$following_repo->findBy(['followed'=>$user]);
        $publicaciones=$publications_repo->findBy(['user'=>$user]);
        $likes=$likes_repo->findBy(['user'=>$user]);
        
        $num_siguiendo= count($siguiendo);
        $num_seguidores= count($seguidores);
        $num_publicaciones= count($publicaciones);
        $num_likes= count($likes);
        
        $result=['following'=>$num_siguiendo,
            'followers'=>$num_seguidores,
            'publications'=>$num_publicaciones,
            'likes'=>$num_likes,
        ];
        
        return $result;
        
   
    }
}
