<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Symfony\Bridge\Doctrine\RegistryInterface;

use App\Entity\Users;

class GetUserExtension extends AbstractExtension
{
    protected $doctrine;

    public function __construct(RegistryInterface $doctrine) {
        $this->doctrine=$doctrine;
    }

    public function getFilters()
    {
        return [
            new TwigFilter('get_user', [$this, 'getUserFilter']),
        ];
    }

    public function getUserFilter($user_id)
    {
        $user_repo=$this->doctrine->getRepository(Users::class);
        
        $user=$user_repo->findOneBy(['id'=>$user_id]);
        
        if(!empty($user) && is_object($user)){
            
            $result=$user;
        }else{
            $result=false;
        }
        
        return $result;
    }
}
