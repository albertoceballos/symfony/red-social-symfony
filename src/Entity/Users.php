<?php

namespace App\Entity;

//necesario para trabajar con la seguridad, cifrado de password:
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Users
 *
 * @ORM\Table(name="users", uniqueConstraints={@ORM\UniqueConstraint(name="nick", columns={"nick"}), @ORM\UniqueConstraint(name="email", columns={"email"})})
 * @ORM\Entity(repositoryClass="App\Repository\UsersRepository")
 */
class Users implements UserInterface, \Serializable {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=20, nullable=false)
     */
    private $role;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="El email no puede estar vacío")
     * @Assert\Email(
     *     message = "El email '{{ value }}' no es un e-mail válido.",
     *     checkMX = false
     * )
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="El nombre no puede estar vacío")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="Los apellidos no pueden estar vacío")
     */
    private $surname;

    /**
     * @var string
     *
     * @ORM\Column(name="nick", type="string", length=50, nullable=false)
     * @Assert\NotBlank(message="El nick no puede estar vacío")
     * @Assert\Length(min=3, minMessage="El nick debe tener al menos {{ limit }} caracteres.")
     */
    private $nick;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="La contraseña no puede estar vacía")
     * @Assert\Length(min=5, minMessage="La contraseña debe tener al menos {{ limit }} caracteres.")
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="active", type="string", length=2, nullable=false)
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(name="bio", type="string", length=255, nullable=false)
     */
    private $bio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    public function getId() {
        return $this->id;
    }

    public function getRole() {
        return $this->role;
    }

    public function setRole(string $role) {
        $this->role = $role;

        return $this;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail(string $email) {
        $this->email = $email;

        return $this;
    }

    public function getName() {
        return $this->name;
    }

    public function setName(string $name) {
        $this->name = $name;

        return $this;
    }

    public function getSurname() {
        return $this->surname;
    }

    public function setSurname(string $surname) {
        $this->surname = $surname;

        return $this;
    }

    public function getNick() {
        return $this->nick;
    }

    public function setNick(string $nick) {
        $this->nick = $nick;

        return $this;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setPassword(string $password) {
        $this->password = $password;

        return $this;
    }

    public function getActive() {
        return $this->active;
    }

    public function setActive(string $active) {
        $this->active = $active;

        return $this;
    }

    public function getBio() {
        return $this->bio;
    }

    public function setBio(string $bio) {
        $this->bio = $bio;

        return $this;
    }

    public function getImage() {
        return $this->image;
    }

    public function setImage($image) {
        $this->image = $image;

        return $this;
    }

    public function eraseCredentials() {
        
    }

    public function getUsername() {
        return $this->getEmail();
    }

    public function getRoles() {
        return array('ROLE_USER');
    }

    public function getSalt() {
        return null;
    }

    public function serialize() {
        return serialize([
            $this->id,
            $this->email,
            $this->password,
        ]);
    }
    
    public function unserialize($serialized) {
        list( 
            $this->id,
            $this->email,
            $this->password,
        )= unserialize($serialized);
    }

}
