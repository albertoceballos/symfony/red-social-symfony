<?php

namespace App\Repository;

use App\Entity\Users;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Users|null find($id, $lockMode = null, $lockVersion = null)
 * @method Users|null findOneBy(array $criteria, array $orderBy = null)
 * @method Users[]    findAll()
 * @method Users[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsersRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Users::class);
    }
    
    
    public function comprobarEmailNick($email,$nick){
        
        $em= $this->getEntityManager();
        $dql= $em->createQuery('SELECT u FROM App\Entity\Users u WHERE u.email=:email OR u.nick=:nick')
                ->setParameter('email',$email)
                ->setParameter('nick',$nick);
                
        return $dql->execute();
    }
    
    public function getFollowingUser($user){
        
        $em= $this->getEntityManager();
        $following_repo= $em->getRepository(\App\Entity\Following::class);
        $following=$following_repo->findBy(['user'=>$user]);
        
        $following_array=[];
        foreach ($following as $follow){
            $following_array[]=$follow->getFollowed();
        }
        
        $users= $this->createQueryBuilder('u')
                ->where('u.id !=:user AND u.id IN (:following)')
                ->setParameter('user', $user->getId())
                ->setParameter('following', $following_array)
                ->orderBy('u.id', 'DESC');
                
        return $users;
        
    }




    // /**
    //  * @return Users[] Returns an array of Users objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Users
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
