$(document).ready(function () {

    var ias = jQuery.ias({
        container: '.box-content',
        item: '.publication-item',
        pagination: '.pagination',
        next: '.next-link',
        triggerPageThreshold: 5
    });

    ias.extension(new IASTriggerExtension({
        offset: 2,
        text: 'Ver más'
    }));

    ias.extension(new IASSpinnerExtension({
        src: URL + '/images/ajax-loader.gif'
    }));

    ias.extension(new IASNoneLeftExtension({
        text: "No hay más publicaciones"
    }));

    ias.on('ready', function (event) {
        followButtons();
    });

    ias.on('rendered', function (event) {
        followButtons();
    });

});

function followButtons() {

    $('.follow-button').unbind('click').click(function () {
        $(this).addClass('d-none');
        $(this).parent().find('.unfollow-button').removeClass('d-none');
        $.ajax({
            url: URL + '/follow',
            type: 'POST',
            data: {followed: $(this).attr("data-followed")},
            success: function (response) {
                console.log(response);
            }
        });
    });


    $('.unfollow-button').unbind('click').click(function () {
        $(this).addClass('d-none');
        $(this).parent().find('.follow-button').removeClass('d-none');
        $.ajax({
            url: URL + '/unfollow',
            type: 'POST',
            data: {followed: $(this).attr("data-followed")},
            success: function (response) {
                console.log(response);
            }
        });
    });


}


