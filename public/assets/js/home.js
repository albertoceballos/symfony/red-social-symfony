$(document).ready(function () {

    var ias = jQuery.ias({
        container: '.box-content',
        item: '.publication-item',
        pagination: '.pagination',
        next: '.next-link',
        triggerPageThreshold: 5
    });

    ias.extension(new IASTriggerExtension({
        offset: 2,
        text: 'Ver más'
    }));

    ias.extension(new IASSpinnerExtension({
        src: URL + '/images/ajax-loader.gif'
    }));

    ias.extension(new IASNoneLeftExtension({
        text: "No hay más publicaciones"
    }));

    ias.on('ready', function (event) {
        buttons();
    });

    ias.on('rendered', function (event) {
        buttons();
    });

});

function buttons() {
    //activar tooltips de bootstrap:
    $('[data-toggle="tooltip"]').tooltip();
    
    //ocultar o mostrar las imágenes de las publicaciones:
    $('.btn-image').unbind('click').click(function () {
        $(this).parent().find('.pub-image').slideToggle();
        $(this).toggleClass('fa-eye-slash');
        $(this).toggleClass('fa-eye');
    });

    //Borrar publicación:
    $('.btn-remove-pub').unbind('click').click(function () {
        var confirma = confirm("¿Quieres borrar esta publicación?");
        if (confirma) {
            $(this).parents('.publication-item').hide();
            $.ajax({
                url: URL + '/remove/' + $(this).attr('data-id'),
                type: 'GET',
                success: function (response) {
                    $('.alert-success').html(response);
                    console.log(response);
                }
            });
        }
    });
    
    //Pulsar like
    $('.btn-like').unbind('click').click(function(){
       $(this).addClass('d-none');
       $(this).parent().find('.btn-dislike').removeClass('d-none');
       
       $.ajax({
          url: URL + '/like/'+ $(this).attr('data-id'),
          type: 'GET',
          success: function (response){
              console.log(response);
          }
       });
    });
    
    //Pulsar dislike
    $('.btn-dislike').unbind('click').click(function(){
       $(this).addClass('d-none');
       $(this).parent().find('.btn-like').removeClass('d-none');
       
       $.ajax({
          url: URL + '/dislike/'+ $(this).attr('data-id'),
          type: 'GET',
          success: function (response){
              console.log(response);
          }
       });
    });
    
}

