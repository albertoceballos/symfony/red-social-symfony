$(document).ready(function () {

    notifications();
    setInterval(notifications, 10000);


});

function notifications() {
    $.ajax({
        url: URL + '/notifications/count',
        type: 'GET',
        success: function (response) {
            $('.num-notifications').html(response);
            if ($('.num-notifications').text() == 0) {
                $('.num-notifications').addClass('d-none');
                $('.notification-li').removeClass('active');
            } else {
                $('.num-notifications').removeClass('d-none');
                 $('.notification-li').addClass('active');
            }
        }
    });
    
    $.ajax({
        url: URL + '/private_messages/notification/get',
        type: 'GET',
        success: function (response){
            $('.num-notifications-msg').html(response);
            if ($('.num-notifications-msg').text() == 0) {
                $('.num-notifications-msg').addClass('d-none');
                $('.message-li').removeClass('active');
            } else {
                $('.num-notifications-msg').removeClass('d-none');
                $('.message-li').addClass('active');
            }
        }
        
    });
    
}


$('.btn-close-notification').unbind('click').click(function () {

    var id_notification = $(this).attr('data-id');

    $.ajax({
        url: URL + '/notifications/delete/' + id_notification,
        type: 'GET',
        success: function (response) {
            console.log(response);
        }
    });

});


